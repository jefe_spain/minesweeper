
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MineSweeper</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
<h1>MineSweeper Selection</h1>
	<form action="generate.php" method="post">

		<input type="number" name="field_width" min="1" max="25" required>
		<input type="number" name="field_height" min="1" max="25" required>
		<input type="number" name="mines" min="1" max="255" required>
		<input type="submit" value="Submit">

	</form>


</body>
</html>



<?php 

$filas = 50;
$columnas = 50 ;
$minas =  450;


$tablero = fill_board($filas, $columnas, $minas);
$tablero = check_fields($tablero);
show_board($tablero);








/**
-- Funcion fill_board(param1,param2,param3) --
Funcion para rellenar el tablero de minas dadas
@param1 int $num_rows Primer parametro, numero de filas del tablero.
@param2 int $num_cols Segundo parametro, numero de columnas del tablero.
@param3 int $mines Tercer parametro , numero de minas a colocar.
@return array  Retorna array multidimensional.

**/
function fill_board($num_rows, $num_cols, $mines)
{

$board = array(array());

for ($f=0; $f < ($num_rows) ; $f++) { 
		for ($c=0; $c < ($num_cols) ; $c++) { 
			$board[$f][$c] = "0";
		}
	}


$i = 1;
while ($i <= $mines && $i <= ($num_rows * $num_cols)){
	$pos_x = mt_rand(0,($num_rows - 1)); // Posicion aleatoria en el eje X del tablero
	
	$pos_y = mt_rand(0,($num_cols - 1)); // Posicion aleatoria en el eje Y del tablero

	if (($board[$pos_x][$pos_y]) != "X") { //Asignamos mina a la posicion seleccionada si se encuentra vacia.
		$board[$pos_x][$pos_y] = "X"; 
		$i++;
	}else
	{

	}
}

return $board;


} // End of the function fill_board


/**
-- Funcion check_board($param1) --



**/





/**
-- Funcion show_board($param1) --


**/

function show_board($board)
{
	echo "<table>";
	foreach ($board as $fila => $valor) {
		echo "<tr>";
		foreach ($valor as $columna => $real) {
			if ($board[$fila][$columna] == "0") {
				echo "<td></td>";
				
			}else
			{
			echo "<td>".$board[$fila][$columna]."</td>";
			};
		}
		
	}


	echo "</table>";
} // End of show_board

/**
-- Funcion check_fields($board) --


**/
function check_fields($board)
{
	$directions = array([-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]); // All the directions availables for the adjacents fields. Clockwise,start from bottom-left.
	$dx = 0; // 
	$dy = 1; //
	foreach ($board as $fila => $valor) {
		;
		foreach ($valor as $columna => $real) {
			if ($board[$fila][$columna] == "X")
			{
				for ($i = 0; $i < 8; $i++) 
					{
						
						if (isset($board[$fila + $directions[$i][$dx]][$columna + $directions[$i][$dy]]))

						{
							if ($board[$fila + $directions[$i][$dx]][$columna + $directions[$i][$dy]] != "X")
								{
								$board[$fila + $directions[$i][$dx]][$columna + $directions[$i][$dy]] += 1;
								};
						};

					}




			}
		}
		
	}




return $board;

}



 ?>

